/*
Getting all the ressource needed to start the bot : 
Node Modules and Configuration
*/
const Discord = require('discord.js');
const client = new Discord.Client({
    ws: { 
        intents: [
            "GUILDS",
            "GUILD_MEMBERS",
            "GUILD_BANS",
            "GUILD_EMOJIS",
            "GUILD_INTEGRATIONS",
            "GUILD_WEBHOOKS",
            "GUILD_INVITES",
            "GUILD_VOICE_STATES",
            "GUILD_PRESENCES",
            "GUILD_MESSAGES",
            "GUILD_MESSAGE_REACTIONS",
            "GUILD_MESSAGE_TYPING",
            "DIRECT_MESSAGES",
            "DIRECT_MESSAGE_REACTIONS",
            "DIRECT_MESSAGE_TYPING"
        ] 
    },
    restTimeOffset: 0,
    disableMentions: 'everyone',
    messageCacheMaxSize: 100
})
const fs = require("fs");
client.config = require('./config.json')
const { updateClient } = require('gunibot')

/*
Create collections for commands and aliases (the command and alias will be there)
*/
client.commands = new Discord.Collection()
client.plugins = new Discord.Collection()
client.alias = new Discord.Collection()

updateClient(client)

/*
If error, send it
When the client is ready log a fancy message with some informations 
    Name of the bot, Id of the bot, Version, Owner, Initiales Dev, Number of server is on, Number of members he have in cache
It also set his activity to "s-help・ <servers number> Servers" and refresh it all 1 minute
*/
client.on("error", console.error)
client.on("ready", async () => {
    let owners = []
    client.config.owners.forEach(owner => {
        client.users.fetch(owner)
        owners.push(client.users.cache.get(owner).tag)
    })
    console.log(`┌──────────── ${client.user.username} ─────────────\n` +
                `│          Name: ${client.user.username}           \n` +
                `│          ID: ${client.user.id}                   \n` +
                "│          Version: 0.0.2                          \n" +
                "│          Owner: Altearn                          \n" +
                "│          Instance Owner: "+owners.join(', ')+"\n" +
                "│          Devs: fantomitechno 🦊#5973,            \n" +
                "│                NewGlace#2905                     \n" +
                "│                and TrétinV3#7056                 \n" +
                `│          Serveurs: ${client.guilds.cache.size}   \n` +
                `│          Membres: ${client.users.cache.size}     \n` +
                "│          For Gunivers/Altearn                    \n" +
                `└──────────── ${client.user.username} ─────────────`)
    
    if (client.config.presence.activated) {
        client.user.setPresence(client.config.presence.list[0]);
        let i = 1
        setInterval(function() {
            client.user.setPresence(client.config.presence.list[i]);
            i ++
            if (i === client.config.presence.list.length) i = 0
        },
        client.config.presence.time)
    }
});


/*
Read the directory /plugins/ and collect all the events and commands
There is some return the first is for files without the extensions .js, the second is for folder (to don't send the error because it's not very beautiful to see "Canno't acces the file <folder> because he isn't in ".js".")
*/
fs.readdir('./plugins/', (err, plugins) => {
    if (err) console.log(err)
    plugins.forEach(plugin => {
        try {
            fs.readdir('./plugins/'+plugin+'/', (err, f) => {
                if (!f.includes('plugin.json')) return console.log(`The plugin ${plugin} can't be loaded, because it have no 'plugin.json'`)
                const plug = require(`./plugins/${plugin}/plugin.json`); //ça fait beaucoup de plugin non ?
                let haveDependences = true;
                if(plug.dependences) plug.dependences.forEach( d => {
                    if(!plugins.includes(d)) haveDependences = false;
                });
                if(!haveDependences) return console.log(`The plugin ${plugin} can't be loaded, because it don't have his dependences`)
                client.plugins.set(plugin, plug);
                if (f.includes('index.js')){
                    const pluginIndex = require(`./plugins/${plugin}/index.js`);
                    pluginIndex.run(client);
                }
                console.log('Succefuly loaded the plugin : '+plugin)
                if (f.includes('commands')) {
                    fs.readdir("./plugins/"+plugin+"/commands/", (err, files) => {
                        if (err) console.log(err)
                        console.log(`\nLoading ${plugin} plugin :`)
                        console.log(`   Loading commande :`)
                        files.forEach(async file => {
                            if (!file.endsWith(".js") && file.includes('.')) return console.log("Je n'ai pas pu accepter au fichier " + file + " car il n'est pas en \".js\" .")
                            if (!file.includes('.')) return
                            let props = require("./plugins/"+plugin+"/commands/" + file)
                            if (!props.help) return console.log("Je n'ai pas pu accepter au fichier " + file + " car il n'est pas complet .")
                            props.help.tag = plugin;
                            if(!props.help.group) props.help.group = plugin;
                            client.commands.set(props.help.name, props)
                            if (typeof(props.help.aliases) === "string") {
                                client.alias.set(props.help.aliases, props.help.name)
                            } else if (typeof(props.help.aliases) === "object") {
                                for (let i = 0; i < props.help.aliases.length; i++) {
                                    client.alias.set(props.help.aliases[i], props.help.name)
                                }
                            }
                            console.log(`      └──📄 ${file}`)
                        })
                    })
                } else console.log(`The plugin ${plugin} has no commands`)
                if (f.includes('events')) {
                    fs.readdir(`./plugins/${plugin}/events/`, (err, files) => {
                        if (err) console.log(err)
                        console.log(`   Loading events :`);
                        files.forEach(file => {
                            if (!file.endsWith(".js") && file.includes('.')) return console.log(`Je n'ai pas pu accepter au fichier ${file} car il n'est pas en ".js" .`)
                            if (!file.includes('.')) return
                            let event = require(`./plugins/${plugin}/events/${file}`)
                            client.on(file.split(".")[0], event.bind(null, client))
                            delete require.cache[require.resolve(`./plugins/${plugin}/events/${file}`)]
                            console.log(`      └──📄 ${file}`)
                        })
                    })
                } else console.log(`The plugin ${plugin} has no events`)
            })
        }
        catch {
            console.log(`Le plugin ${plugin} n'as pas pu être chargé`)
        }
    })
})


/*
Log the client with the token in setup.js
*/
client.login(client.config.token);
